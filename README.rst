******************************
Data Structures and Algorithms
******************************

###################
A cheat-sheet - WIP
###################

.. contents::
..
    1  Dynamic Arrays
    2  Linked Lists
    3  Trees
    4  Hash Maps
    5  Tries
    6  Graphs

For each data structure try to describe:

- general importance
- when and why to use
- specific importance for a class of problems
- how a naive implementation would look like
- graphical representation
- well-known implementations if applicable e.g: custom trees
- well-known algorithms that use this data structure e.g: sorting algos
- typical operations for the data structure with code sample and complexity


--------------
Dynamic Arrays
--------------

- WIP
- Probably the most common DS
- Used to represent multidimensional arrays, strings, pairs, etc
- Contiguous in memory (in theory)
- Random indexing in O(1)
- Resizing is rather expensive, consisting of allocating new memory
  and copying the data
- Usually represented as a pointer to a chunk of memory and
  the number of elements in that chunk

.. code-block:: c

   struct Vector
   {
       int   *ptr;
       size_t len;
   };

::

   // ┌───────┬───────┬───────┬───────┬───────┬───────┐
   // │       │       │       │       │       │       │
   // │ value │ value │ value │ value │ value │ value │
   // │       │       │       │       │       │       │
   // └───────┴───────┴───────┴───────┴───────┴───────┘

Typical scenarios and operations:

- Sorting

  - Usually targeting O(Nlog(N)), but they degenerate in certain scenarios
  - Most algorithms work on contiguous memory
  - Common algorithms:

    - `Quick sort <https://en.wikipedia.org/wiki/Quicksort#Algorithm>`_

      - Divide et impera style of algorithm
      - **Can degenerate** to O(N²) if the ``pivot`` is not chosen optimally
      - If implemented correctly it is faster than both Merge sort and Heap sort

    - `Merge sort <https://en.wikipedia.org/wiki/Merge_sort#Algorithm>`_

      - Typically uses auxiliary storage O(N)
      - Divide et impera style of algorithm
      - Can work on **non-contiguous memory**
      - Can use external memory (tape, disks, etc) to hold sorted chunks
      - **Stable sort** i.e. the original order of equal items is kept
        in the sorted output

    - `Radix sort <https://en.wikipedia.org/wiki/Radix_sort>`_

      - **Non-comparative** sorting algorithm
      - Rather **domain specific**
      - Elements are distributed into *buckets* according to their radix
      - Very useful when the number of buckets is relatively small and/or
        known beforehand
      - Can be implemented using a trie and pre-order traversal

    - `Heap sort <https://en.wikipedia.org/wiki/Heapsort>`_

      - Maps a complete binary tree to an array i.e. the Heap data structure
      - The heap property is that given **any node N its parent P is
        greater(max) or smaller(min) than N**
      - A heap can be max (the root is the greatest element) or min
      - Determine the median in a stream
      - Sort multiple arrays
      - Top frequent elements
      - Left child is ``2 * parent_index + 1``
      - Right child is ``2 * parent_index + 2``
      - Parent is ``child_index / 2``

      .. code-block:: cpp

         void
         bubble_down(std::size_t idx, std::vector<int> &vals, std::size_t n)
         {
             if (idx >= n) {
                 return;
             }

             const auto orig  = idx;
             auto       left  = 2 * idx + 1;
             auto       right = 2 * idx + 2;

             if (left < n && vals.at(left) < vals.at(idx)) {
                 idx = left;
             }

             if (right < n && vals.at(right) < vals.at(idx)) {
                 idx = right;
             }

             if (idx != orig) {
                 std::swap(vals.at(orig), vals.at(idx));
                 bubble_down(idx, vals, n);
             }
         }

         void
         heapify(std::vector<int> &vals, std::size_t len)
         {
             for (std::size_t i = len / 2; i > 0; --i) {
                 bubble_down(i, vals, len);
             }
             bubble_down(0, vals, len);
         }

         void
         heapsort(std::vector<int> &vals)
         {
             heapify(vals, vals.size());

             for (std::size_t i = 1; i < vals.size(); ++i) {
                 std::swap(vals.at(0), vals.at(vals.size() - i));
                 bubble_down(0, vals, vals.size() - i);
             }
         }

- Binary search

  - Given a sorted array one can search in O(log(N)) for an element
  - Every time one hears sorted array one should think if a modified binary
    search could solve the problem
  - A similar logic is used on BSTs to find an element

  .. code-block:: cpp

     bool
     binary_search(const std::vector<int> &vals,
                   std::size_t             start,
                   std::size_t             end,
                   const int               value)
     {
         if (start > end) {
             return false;
         }

         const auto  mid     = (start + end) / 2;
         const auto  mid_val = vals.at(mid);

         if (mid_val == value) {
             return true;
         } else if (value < mid_val) {
             return binary_search(vals, start, mid - 1, value);
         } else {
             return binary_search(vals, mid + 1, end, value);
         }
     }


- Backtracking TODO

- Dynamic Programming TODO


- Subsets

  - Also called super-set
  - ``[1, 2, 3] -> [ [], [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3] ]``
  - Can be easily modified to meet certain conditions such as
    starting from arrays with duplicates, generating combinations, etc

  .. code-block:: cpp

     void
     subset_backtrack(std::vector<int>              &vals,
                      std::vector<int>              &tmp,
                      std::vector<std::vector<int>> &sol,
                      std::size_t                    start)
     {
         sol.push_back(tmp);
         for (std::size_t i = start; i < vals.size(); ++i) {
             tmp.push_back(vals.at(i));
             subset_backtrack(vals, tmp, sol, i + 1);
             tmp.pop_back();
         }
     }

     void
     subsets(std::vector<int> &vals, std::vector<std::vector<int>> &solutions)
     {
         std::sort(vals.begin(), vals.end());
         std::vector<int> tmp;
         subset_backtrack(vals, tmp, solutions, 0);
     };


- Permutations

  - Heap's algorithm
  - The kind of solution that you just memorise

  .. code-block:: cpp

     void
     permutations(std::vector<int>              &vals,
                  std::vector<std::vector<int>> &solutions,
                  std::size_t                    end)
     {
         if (end == 1) {
             solutions.push_back(vals);
         } else {
             for (size_t i = 0; i < end; ++i) {
                 permutations(vals, solutions, end - 1);
                 if (i % 2) {
                     std::swap(vals.at(i), vals.at(end - 1));
                 } else {
                     std::swap(vals.at(0), vals.at(end - 1));
                 }
             }
         }
     }

- Two pointer indexing

  - Determine if any two numbers in a sorted array amount to a certain sum
  - The algorithm can be generalised for more than two pointers

  .. code-block:: cpp

     std::pair<size_t, size_t>
     two_ptr(const std::vector<int> &vals, const int target)
     {
         size_t i = 0;
         size_t j = vals.size() - 1;

         while (i < j) {
             auto sm = vals.at(i) + vals.at(j);
             if (sm == target) {
                 return { i, j };
             } else if (sm < target) {
                 ++i;
             } else {
                 --j;
             }
         }
         return { 0, 0 };
     }

- Sliding window

  - a method of validating if a sub-interval of a fixed size (window) meets a
    certain criteria
  - the idea is that not the actual values interest us, but their sum or
    something similar
  - thus whenever the window slides we don't recompute the entire value, but
    just remove the oldest value and insert the newest

  .. code-block:: cpp

     std::pair<size_t, size_t>
     find_window(const std::vector<int> &vals, size_t window_size, int target_sum)
     {
         if (vals.size() < window_size) {
             return { 0, 0 };
         }

         size_t window_val =
             std::accumulate(vals.begin(), vals.begin() + window_size, 0);

         for (size_t i = 0, j = window_size; j < vals.size(); ++i, ++j) {
             if (window_val == target_sum) {
                 return { i, j };
             } else {
                 window_val = window_val + vals.at(j) - vals.at(i);
             }
         }

         return { 0, 0 };
     }


------------
Linked Lists
------------

- Non-contiguous list of items
- Two nodes and a unidirectional (sometimes bi) link between them
- Commonly used when we have lots of items but we don't traverse them often
- One of (if not) the most important data structure because basically almost
  all the others use this one as a building block

.. code-block:: c

   struct Node
   {
       int          value;
       struct Node *next;
   };


::

   //   ┌─────────┐        ┌─────────┐       ┌─────────┐
   //   │  value  │        │  value  │       │  value  │
   //   │         │        │         │       │         │
   //   │         │        │         │       │         │
   //   │  next   ├───────►│  next   ├──────►│  next   ├──────►NULL
   //   └─────────┘        └─────────┘       └─────────┘

Typical scenarios and operations:

- Queue data structure

  - First in, first out FIFO
  - Insertion happens at the end of the list
  - Deletion takes place at the front of the list

- Stack data structure

  - Last in, first out LIFO
  - Insertion takes place at the front of the list
  - Deletion takes place at the front of the list

- Find random element - O(N)
- Delete random element - O(N)

  .. code-block:: c

     struct Node *delete (struct Node *head, int (*cond)(struct Node *))
     {
         struct Node *prev    = NULL;
         struct Node *current = head;

         while (current && !cond(current)) {
             prev    = current;
             current = current->next;
         }

         /* not found */
         if (!current) {
             return head;
         }

         /* delete first element */
         if (current == head) {
             head = head->next;
         } else { /* delete any other element */
             prev->next = current->next;
         }

         free(current);
         return head;
     }

- Reverse list

  .. code-block:: c

     struct Node *
     reverse(struct Node *prev, struct Node *current)
     {
         if (!current) {
             return prev;
         }

         struct Node *next = current->next;
         current->next     = prev;

         return reverse(current, next);
     }

- Identify is a list has a cycle - Floyd’s Cycle-Finding Algorithm

  .. code-block:: c

     int
     has_cycle(struct Node *head)
     {
         struct Node *slow = head;
         struct Node *fast = head;

         while (slow && fast && fast->next) {
             slow = slow->next;
             fast = fast->next->next;
             if (slow == fast) {
                 return 1;
             }
         }

         return 0;
     }


-----
Trees
-----

- Basically a linked list with usually two links (left and right) representing
  its children
- Links that have no children are called leaves
- Most common application is that of ordering and searching for items e.x. maps,
  dictionaries
- Note-worthy implementations:

  - Binary search tree (BST)

    One such that the left sub-tree is smaller and the right sub-tree is greater
    than their parent

  - Self-balancing BSTs

    Needed because as we insert new items into the tree any of the two subtrees
    can overwhelm the other in depth and future searches would not yield
    optimal time performance O(log(N))

    - `AVL <https://en.wikipedia.org/wiki/AVL_tree>`_
    - `Red-black tree <https://en.wikipedia.org/wiki/Red%E2%80%93black_tree>`_
    - `B-tree <https://en.wikipedia.org/wiki/B-tree>`_
    - `Splay-tree <https://en.wikipedia.org/wiki/Splay_tree>`_

.. code-block:: c

   struct Node
   {
       int value;

       struct Node *left;
       struct Node *right;
   };


::

//                                         ┌──────────┐
//                                         │  value   │
//                                         │          │
//                                         │          │
//                           ┌─────────────┤left|right├──────────────┐
//                           │             └──────────┘              │
//                           │                                       │
//                     ┌─────▼────┐                             ┌────▼─────┐
//                     │  value   │                             │  value   │
//                     │          │                             │          │
//                     │          │                             │          │
//              ┌──────┤left|right├─┐                    ┌──────┤left|right├──────┐
//              │      └──────────┘ │                    │      └──────────┘      │
//              │                   │                    │                        │
//         ┌────▼─────┐             ▼               ┌────▼─────┐            ┌─────▼────┐
//         │  value   │            NULL             │  value   │            │  value   │
//         │          │                             │          │            │          │
//         │          │                             │          │            │          │
//    ┌────┤left|right├────┐                   ┌────┤left|right├──┐      ┌──┤left|right├───┐
//    │    └──────────┘    │                   │    └──────────┘  │      │  └──────────┘   │
//    │                    │                   │                  │      │                 │
//    ▼                    ▼                   ▼                  ▼      ▼                 ▼
//   NULL                 NULL                NULL               NULL   NULL              NULL


Typical scenarios and operations:

- Traversals

  - Depth-first traversal

    - Inorder

      - In case of a BST it yields the values in non-decreasing order i.e.
        can test if a tree is a BST

      .. code-block:: c

         void
         inorder(struct Node *head, void (*op)(struct Node *))
         {
             inorder(head->left, op);
             op(head);
             inorder(head->right, op);
         }

    - Preorder

      - Used to search for a subtree in another tree

      .. code-block:: c

         void
         preorder(struct Node *head, void (*op)(struct Node *))
         {
             op(head);
             inorder(head->left, op);
             inorder(head->right, op);
         }


    - Postorder

      - Used to delete a tree

      .. code-block:: c

         void
         postorder(struct Node *head, void (*op)(struct Node *))
         {
             inorder(head->left, op);
             inorder(head->right, op);
             op(head);
         }


  - Breadth-first traversal

    - Used to visit each level in a tree
    - Can be used to print leftmost/rightmost elements on each level
    - Typically implemented using a queue data structure

    .. code-block:: cpp

       void
       level_order(struct Node *head, std::function<void(struct Node *)> op)
       {
           std::deque<struct Node *> q;

           q.push_back(head);

           while (q.size() > 0) {
               struct Node *current = q.front();
               q.pop_front();

               if (current) {
                   op(current);
                   q.push_back(current->left);
                   q.push_back(current->right);
               }
           }
       }

- Searching in a BST O(log(N))

.. code-block:: c

   int
   search(struct Node *head, int value)
   {
       if (!head) { /* end */
           return 0;
       } else if (head->value == value) { /* found */
           return 1;
       } else if (value < head->value) { /* go left */
           return search(head->left, value);
       } else { /* go right */
           return search(head->right, value);
       }
   }

---------
Hash Maps
---------

- used to map a key to a value
- ``hash(key) -> value``
- the insert and find operations should be  very fast O(1)
- **keys are not ordered** compared to a BST
- **collisions are possible** because each key is ``hashed`` with a function
  that usually yields a number of fixed width
- the most basic implementation is in the form of a fixed array of linked
  list

.. code-block:: cpp

   class HashMap
   {
   public:
       using value_t = int;
       using key_t   = int;

   private:
       struct Node
       {
           const key_t  k;
           value_t      v {};
           struct Node *next {};
       };

       static constexpr size_t arr_size { 17 };
       struct Node            *arr[arr_size] {};
   };


::

   // ┌──────┐   ┌──────┐   ┌──────┐
   // │      │   │      │   │      │
   // │ K,V  ├───► K,V  ├───► K,V  ├───►NULL
   // │      │   │      │   │      │
   // ├──────┤   └──────┘   └──────┘
   // │      │
   // │ K,V  ├───►NULL
   // │      │
   // ├──────┤   ┌──────┐
   // │      │   │      │
   // │ K,V  ├───► K,V  ├───►NULL
   // │      │   │      │
   // ├──────┤   ├──────┤   ┌──────┐
   // │      │   │      │   │      │
   // │ K,V  ├───► K,V  ├───► K,V  ├──►NULL
   // │      │   │      │   │      │
   // ├──────┤   └──────┘   └──────┘
   // │      │
   // │ K,V  ├───►NULL
   // │      │
   // └──────┘

- whenever two keys collide we use the linked list to append the colliding entry
- note that **a fixed-size array or a weak hash function will severely degrade
  the performace of the hash map**

Typical scenarios and operations:

- Insert key-value pair

  .. code-block:: cpp

     void
     insert(key_t k, value_t v)
     {
         // using key mod prime_number is a very rudimentary hashing method and
         // not recommended for real cases
         auto &&node = arr[k % arr_size];

         if (!node) {
             node = new Node { k, v, nullptr };
         } else {
             // check if key already exists
             auto i = node;
             while (i && i->k != k) {
                 i = i->next;
             }

             // if key exists overwrite value
             if (i) {
                 i->v = v;
             } else { // else create new node and place at head
                 i    = new Node { k, v, node };
                 node = i;
             }
         }
     }


- Retrieve value corresponding to key

  .. code-block:: cpp

     bool
     find(key_t k, value_t &val)
     {
         auto node = arr[k % arr_size];

         while (node && node->k != k) {
             node = node->next;
         }

         if (node) {
             val = node->v;
             return true;
         } else {
             return false;
         }
     }

- in the above example we cheat and replace the hash function with
  a modulo operand because we defined the array of size prime number
- usually one relies on the hash maps and/or hashing functions of the
  standard library of the programming language in question


-----
Tries
-----

- WIP
- tries are a special flavour of trees where each node usually has a known
  maximum number of children which represent a value
- any path from root to leaf represents an entry
- some paths from root to non-leaf nodes *may* represent complete entries
- path compression is commonly implemented to alleviate storage
  requirements when successive nodes that have the same value
- commonly used in:

  - routing tables because we know the maximum size
    of an IP address and it's possible values:
    `Poptrie <https://conferences.sigcomm.org/sigcomm/2015/pdf/papers/p57.pdf>`_
  - string searching algorithms because of the fixed dictionary
    and possible substrings:
    `Radix trie <https://en.wikipedia.org/wiki/Radix_tree>`_,
    `Aho-Corasick\
    <https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm>`_

- trivially represented as a fixed-size array of pointers to self

.. code-block:: cpp

   class Trie
   {
       // lowercase english alphabet
       static constexpr size_t key_domain_size = 26 * 2;

       struct Node
       {
           const char value;
           bool       limit {};
           Node      *arr[key_domain_size] {};
       };

       Node *arr[key_domain_size] {};
   };

::

   //                      ┌─────┐
   //                      │     │
   //        ┌─────────────┤  P  ├────────────┐
   //        │             └──┬──┘            │
   //        │                │               │
   //     ┌──▼──┐          ┌──▼──┐         ┌──▼──┐
   //     │     │          │     │         │     │
   //     │  L  │          │  O  │         │  A  │
   //     └──┬──┘          └──┬──┘         └──┬──┘
   //        │                │               │
   //     ┌──▼──┐          ┌──▼──┐         ┌──▼──┐
   //     │     │          │     │         │     │
   //   ┌─┤  A  ├─┐      ┌─┤  S| ├─┐       │  R  │
   //   │ └─────┘ │      │ └─────┘ │       └──┬──┘
   //   │         │      │         │          │
   // ┌─▼───┐ ┌───▼─┐  ┌─▼───┐ ┌───▼─┐     ┌──▼──┐
   // │     │ │     │  │     │ │     │     │     │
   // │  N| │ │  Y| │  │  T| │ │  E| │     │  T| │
   // └─────┘ └─────┘  └─────┘ └─────┘     └──┬──┘
   //                                         │
   //                                      ┌──▼──┐
   //                                      │     │
   //                                      │  Y| │
   //                                      └─────┘

- the above diagram is not true to how we defined the code for the structure
  because we did not represent each level as an array of pointers to nodes
- each node that has a vertical bar ``|`` is a complete entry

Typical scenarios and operations:

- Insertion

  .. code-block:: cpp

     void
     insert(const std::string &value)
     {
         auto  current = arr;
         Node *last_node {};
         for (auto c : value) {
             auto &node = current[c - 'a'];
             if (!node) {
                 node = new Node { c };
             }
             last_node = node;
             current   = node->arr;
         }
         if (last_node) {
             last_node->limit = true;
         }
     }


- Check if element exists

  .. code-block:: cpp

     bool
     exists(const std::string &value)
     {
         auto        current = arr;
         const Node *last_node {};
         for (auto c : value) {
             const auto &node = current[c - 'a'];
             if (!node) {
                 return false;
             }
             last_node = node;
             current   = node->arr;
         }

         if (last_node && last_node->limit) {
             return true;
         } else {
             return false;
         }
     }


- Check if prefix exists

  .. code-block:: cpp


     bool
     prefix(const std::string &pre)
     {
         auto current = arr;
         for (auto c : pre) {
             const auto &node = current[c - 'a'];
             if (!node) {
                 return false;
             }
             current = node->arr;
         }

         return true;
     }


------
Graphs
------

- Probably the most useful data structure for real-life modelling
- Used to **represent relationships**\(links, edges, arcs) between entities(nodes, points, vertices)
- Links can be **unidirectional(directed graph)** or **bidirectional(undirected graph)**
- Usually links have **costs or weights** when any two adjacent nodes have multiple links
- Two nodes reaching one another using a single link are called **adjacent**
- The relationship between entities can represent roads, hops, costs, times, etc
- In a naive way one can see them as a tree structure with no roots and leaves, and having loops
- One of the most popular subclass of problems is the **minimum spanning tree**
- For **N nodes** one needs exactly **N-1 links** to connect them all
- All the connected nodes in a graph form a **component**
- Two or more components don't communicate to each other
- Given their versatility graphs have **multiple representations according to the problem at hand**
- The most common representation is the **adjacency list**, which is a rather unfortunate name
  because in most scenarios it's some sort of ``map``
- TODO -- graphs modelled using 2D arrays
- An adjacency list keeps record of all the direct neighbours of any node in the graph

.. code-block:: cpp

   class Graph
   {
   public:
       using node_id        = int;
       using adjacency_list = std::unordered_map<node_id, std::list<node_id>>;

   private:
       adjacency_list neighbours;
   };

::

   //             ┌─────┐    ┌─────┐
   //    ┌─────┬──┤  1  ├────┤  2  │
   //    │     │  │     │    │     │
   //    │     │  └──┬──┘    └──┬──┘
   //    │     │     │          │
   //    │     │     │          │
   // ┌──┴──┐  │  ┌──┴──┐    ┌──┴──┐
   // │  3  │  │  │  4  │    │  5  │
   // │     ├──┼──┤     │    │     │
   // └─────┘  │  └─────┘    └──┬──┘
   //          │                │
   //          │                │
   // ┌─────┐  │  ┌─────┐    ┌──┴──┐
   // │  6  │  │  │  7  ├────┤  8  │
   // │     ├──┘  │     │    │     │
   // └─────┘     └─────┘    └─────┘

- The above sample is used to model a graph that can be *both directed or undirected*, and whose *links
  have the same cost*
- One can replace the ``std::list`` with another ``std::unordered_map`` if the links have costs

Typical scenarios and operations:

- Traversal

  Given that graphs can have loops one needs to **keep track of visited nodes**

  - Depth-first

    - Commonly used when a complete path from source to destination
      might represent a solution
    - Uses a LIFO helper

    .. code-block:: cpp

       void
       depth_first(node_id start, std::function<void(node_id)> process) const
       {
           std::deque<node_id>         order;
           std::unordered_set<node_id> visited;

           if (neighbours.count(start)) {
               order.push_back(start);
           }

           while (!order.empty()) {
               auto current = order.back();
               order.pop_back();

               visited.insert(current);

               process(current);

               for (auto &&neighbour : neighbours.at(current)) {
                   if (visited.count(neighbour) == 0) {
                       order.push_back(neighbour);
                   }
               }
           }
       }


  - Breadth-first

    - Commonly used to determine the minimum cost
    - Search nodes closer to the given source
    - Uses a FIFO helper

    .. code-block:: cpp

       void
       breadth_first(node_id start, std::function<void(node_id)> process) const
       {
           std::deque<node_id>         order;
           std::unordered_set<node_id> visited;

           if (neighbours.count(start)) {
               order.push_back(start);
           }

           while (!order.empty()) {
               auto current = order.front();
               order.pop_front();

               visited.insert(current);

               process(current);

               for (auto &&neighbour : neighbours.at(current)) {
                   if (visited.count(neighbour) == 0) {
                       order.push_back(neighbour);
                   }
               }
           }
       }

  Notice that the only difference between ``depth`` and ``breadth`` is the manner of processing
  the adjacent vertices.

- Topological sorting

  - Used in **task scheduling**
  - A method of solving a **dependency graph**
  - Only possible for **directed acyclic graphs (DAG)**
  - Strictly speaking it's **a method of traversal where a node is visited
    only after the nodes it depends on have been already visited**
  - **Multiple solutions are possible**
  - `Kahn's Algorithm <https://en.wikipedia.org/wiki/Topological_sorting#Kahn's_algorithm>`_

    .. code-block:: cpp

       std::list<node_id>
       kahn() const
       {
           std::list<node_id>                  sol;
           std::unordered_map<node_id, size_t> deps;
           std::deque<node_id>                 no_dep;

           // create a mapping of node -> no of links to node
           for (auto &&[_, v] : neighbours) {
               for (auto &&dest : v) {
                   deps[dest]++;
               }
           }

           // find nodes with zero links pointing to it
           for (auto &&[k, _] : neighbours) {
               if (deps.count(k) == 0) {
                   no_dep.push_back(k);
               }
           }

           while (!no_dep.empty()) {
               auto current = no_dep.front();
               no_dep.pop_front();

               sol.push_back(current);

               auto &&neighs = neighbours.find(current);

               if (neighs != neighbours.end()) {
                   for (auto &&next : neighs->second) {
                       // subtract link from current -> next
                       auto count = --deps.at(next);
                       if (count == 0) {
                           no_dep.push_back(next);
                           deps.erase(next);
                       }
                   }
               }
           }

           if (deps.empty()) {
               return sol;
           } else {
               return {}; // not a DAG
           }
       }


  - Depth-first search algorithm

    .. code-block:: cpp

       bool
       _dfs_visit(node_id                      current,
                  std::unordered_set<node_id> &perm,
                  std::unordered_set<node_id> &tmp,
                  std::list<node_id>          &sol) const
       {
           if (tmp.count(current)) {
               return false;
           }

           tmp.insert(current);

           if (perm.count(current)) {
               return true;
           }

           auto &&neighs = neighbours.find(current);
           if (neighs != neighbours.end()) {
               for (auto &&next : neighs->second) {
                   if (!_dfs_visit(next, perm, tmp, sol)) {
                       return false;
                   }
               }
           }

           perm.insert(current);
           tmp.erase(current);
           sol.push_front(current);

           return true;
       }

       std::list<node_id>
       dfs_topo() const
       {
           std::list<node_id> sol;

           std::unordered_set<node_id> tmp, perm;

           for (auto &&[k, _] : neighbours) {
               if (perm.count(k)) {
                   continue;
               }

               if (!_dfs_visit(k, perm, tmp, sol)) {
                   return {};
               }
           }

           return sol;
       }


- Minimum spanning tree: Kruskal, Djikstra, Bellman-Ford
- Union-find
